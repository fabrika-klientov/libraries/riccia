<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia\Models;


use Riccia\Core\Build\Builder;
use Riccia\Core\Helpers\Base;
use Riccia\Core\Query\HttpClient;

abstract class Model extends Base
{
    /**
     * @var HttpClient $httpClient
     * */
    protected $httpClient;
    /**
     * @var Builder $builder
     * */
    protected $builder;
    /**
     * @var array $links
     * */
    protected $links;
    /**
     * @var array $keys
     * */
    protected $keys;
    /**
     * @var array $expand
     * */
    protected $expand;

    public function __construct(HttpClient $client, array $data = [])
    {
        parent::__construct($data);
        $this->httpClient = $client;
        $this->builder = new Builder();
    }

    /** magic where closure
     * @param string $name
     * @param array $arguments
     * @return $this
     * */
    public function __call($name, $arguments)
    {
        $this->builder->where($name, ...$arguments);

        return $this;
    }

    /**
     * @param array $data
     * @param string $for
     * @return array
     * */
    protected function getPrepareResult(array $data, string $for)
    {
        return empty($this->keys[$for])
            ? []
            : collect($this->keys[$for])->reduce(function ($result, $one) {
                return $result[$one] ?? [];
            }, $data);
    }

}
