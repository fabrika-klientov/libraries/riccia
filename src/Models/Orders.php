<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia\Models;

use Riccia\Core\Collection\Collection;
use Riccia\Models\Helpers\Counts;
use Riccia\Models\Helpers\HasFind;
use Riccia\Models\Helpers\HasList;

/**
 * @property int $id
 * @property int $market_id
 * @property string $created
 * @property string $changed
 * @property string $amount
 * @property string $amount_with_discount
 * @property string $cost
 * @property string $cost_with_discount
 * @property int $status
 * @property int $status_group
 * @property array $items_photos
 * @property array $seller_comment
 * @property string $seller_comment_created
 * @property string $current_seller_comment
 * @property string $comment
 * @property string $user_phone
 * @property string $from_warehouse
 * @property string $ttn
 * @property int $total_quantity
 * @property bool $can_copy
 * @property int $created_type
 * @property bool $is_viewed
 * @property array $chatUser
 * @property array $chatMessages
 * @property array $user
 * @property array $delivery
 * @property array $purchases
 * @property array $status_available
 * @property bool $is_access_change_order
 * @property array $status_data
 * @property string $payment_type
 * @property string $payment_type_name
 * @property array $credit_info
 * @property array $delivery_service
 *
 * @method $this page(int $page)
 * @method $this id(int $id)
 * @method $this item_id(int $item_id)
 * @method $this status(int $status)
 * @method $this delivery_id(int $delivery_id)
 * @method $this created_from(string $created_from)
 * @method $this created_to(string $created_to)
 * @method $this changed_from(string $changed_from)
 * @method $this changed_to(string $changed_to)
 * @method $this userName(string $userName)
 * @method $this user_phone(string $user_phone)
 * @method $this seller_comment(string $seller_comment)
 * @method $this amountFrom(int $amountFrom)
 * @method $this amountTo(int $amountTo)
 * @method $this type(int $type)
 * @method $this is_viewed(bool $is_viewed)
 * @method $this sort(string $sort) id, -id, created, -created, changed, -changed, total_quantity, -total_quantity, amount, -amount, status, -status
 * @method $this expand(array $expand) chatUser, delivery, user, status_available, purchases, total_quantity, is_access_change_order, status_data (see $expand)
 * */
final class Orders extends Model
{
    use HasList, HasFind, Counts;

    protected $links = [
        'list' => '/orders/search',
        'find' => '/orders/',
        'counts' => '/orders/counts',
        'search_delivers' => '/orders/search-delivers',
    ];
    protected $keys = [
        'list' => ['content', 'orders'],
        'find' => ['content'],
        'counts' => ['content'],
        'search_delivers' => ['content', 'search_delivers'],
    ];
    protected $expand = [
        'list' => [
            'chatUser',
            'delivery',
            'user',
            'status_available',
            'purchases',
            'total_quantity',
            'is_access_change_order',
            'status_data',
            'payment_type',
            'payment_type_name',
            'credit_info',
            'delivery_service',
        ],
        'find' => [
            'chatUser',
            'chatMessages',
            'delivery',
            'user',
            'status_available',
            'status_data',
            'purchases',
            'total_quantity',
            'is_access_change_order',
            'payment_type',
            'credit_info',
            'delivery_service',
            'payment_type_name',
        ],
    ];

    /**
     * @return Collection
     * */
    public function searchDelivers()
    {
        $type = 'search_delivers';
        $result = $this->httpClient->get($this->links[$type], []);

        return new Collection($this->getPrepareResult($result ?? [], $type));
    }

}
