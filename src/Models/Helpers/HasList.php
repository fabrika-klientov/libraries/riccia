<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia\Models\Helpers;


use Riccia\Core\Collection\Collection;

trait HasList
{
    /**
     * @return Collection
     * */
    public function get()
    {
        $type = 'list';
        $options = $this->builder->getResult();
        $result = $this->httpClient->get(
            $this->links[$type],
            ['query' => array_merge($options, ['expand' => join(',', $options['expand'] ?? $this->expand[$type])])]
        );

        return new Collection(array_map(function ($item) {
            return new self($this->httpClient, $item);
        }, $this->getPrepareResult($result ?? [], $type)));
    }

}