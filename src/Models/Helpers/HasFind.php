<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia\Models\Helpers;


trait HasFind
{
    /**
     * @param int $id
     * @return self
     * */
    public function find(int $id): self
    {
        $type = 'find';
        $result = $this->httpClient->get($this->links[$type] . $id, ['query' => ['expand' => join(',', $this->expand[$type])]]);

        return new self($this->httpClient, $this->getPrepareResult($result ?? [], $type));
    }

}