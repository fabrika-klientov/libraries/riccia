<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia\Models\Helpers;


trait Counts
{
    /**
     * @return array
     * */
    public function counts()
    {
        $type = 'counts';
        $result = $this->httpClient->get($this->links[$type], []);

        return $this->getPrepareResult($result ?? [], $type);
    }
}