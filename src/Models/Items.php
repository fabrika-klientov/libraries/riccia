<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia\Models;


use Riccia\Models\Helpers\Counts;
use Riccia\Models\Helpers\HasFind;
use Riccia\Models\Helpers\HasList;

/**
 * @property int $id
 * @property string $name
 * @property string $name_ua
 * @property int $market_id
 * @property string $article
 * @property string $price_offer_id
 * @property string $price
 * @property array $catalog_category
 * @property int $catalog_id
 * @property int $group_id
 * @property string $photo_preview
 * @property array $photo
 * @property int $moderation_status
 * @property int $sla_id
 * @property string $url
 * @property int $sold
 * @property string $uploader_offer_id
 * @property int $uploader_status
 * @property int $status
 * @property int $sell_status
 * @property string $description
 * @property string $description_ua
 * @property array $details
 * @property array $options
 * @property mixed $group_item
 * @property array $parent_category
 * @property array $status_available
 *
 * @method $this product_id(int $product_id)
 * @method $this sell_status(int $sell_status)
 * @method $this price_offer_id(int $price_offer_id)
 * @method $this article(string $article)
 * @method $this product_name(string $product_name)
 * @method $this find_by_text(string $find_by_text)
 * @method $this catalog_id(int $catalog_id)
 * @method $this priceFrom(int $priceFrom)
 * @method $this priceTo(int $priceTo)
 * @method $this pricePromoFrom(int $pricePromoFrom)
 * @method $this pricePromoTo(int $pricePromoTo)
 * @method $this item_active(int $item_active)
 * @method $this on_moderation(int $on_moderation)
 * @method $this page(int $page)
 * @method $this price_promo(bool $price_promo)
 * @method $this sort(string $sort) id, -id, name, -name, article, -article, price, -price
 * @method $this expand(array $expand) sell_status, sold, status, description, description_ua, details, group_item, parent_category, status_available, price_promo, is_promo_sent (see $expand)
 * */
final class Items extends Model
{
    use HasList, HasFind, Counts;

    protected $links = [
        'list' => '/items/search',
        'find' => '/items/',
        'counts' => '/items/counts',
    ];
    protected $keys = [
        'list' => ['content', 'items'],
        'find' => ['content'],
        'counts' => ['content'],
    ];
    protected $expand = [
        'list' => [
            'sell_status',
            'sold',
            'status',
            'description',
            'description_ua',
            'details',
            'group_item',
            'parent_category',
            'status_available',
            'price_promo',
            'is_promo_sent',
        ],
        'find' => [
            'sell_status',
            'sold',
            'status',
            'description',
            'description_ua',
            'details',
            'options',
            'group_item',
            'parent_category',
            'status_available',
        ],
    ];
}