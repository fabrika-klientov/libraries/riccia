<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia\Core\Query;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Riccia\Core\Auth\AuthService;

class HttpClient
{
    /**
     * @var AuthService $authService
     * */
    private $authService;
    /**
     * @var string $LINK
     * */
    private static $LINK = 'https://api.seller.rozetka.com.ua/';

    /**
     * @param array $auth
     * @return void
     * */
    public function __construct(array $auth)
    {
        $this->authService = new AuthService($this, $auth);
    }

    /** api GET
     * @param string $link
     * @param array $options
     * @return array|null
     * @throws \Exception
     * */
    public function get($link, $options)
    {
        return $this->request('GET', $link, $options);
    }

    /** api POST
     * @param string $link
     * @param array $options
     * @return array|null
     * @throws \Exception
     * */
    public function post($link, $options)
    {
        return $this->request('POST', $link, $options);
    }

    /** auth data
     * @return array
     * */
    public function getAuth()
    {
        return $this->authService->getAuthData();
    }

    /** full url
     * @param string $link
     * @return string
     * */
    public function getURL(string $link = '')
    {
        return self::$LINK . $link;
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @param bool $force
     * @return array|null
     * @throws \Exception
     * */
    public function request(string $method, string $link, array $options, bool $force = false)
    {
        try {
            $startTime = time();

            $response = $this->getClient()->request($method, $link, $options);
            $resData = $response->getBody()->getContents();
            $result = json_decode($resData, true);

            $this->logging("Request:: [$method] [$link]", [
                'lib' => 'riccia',
                'lib_user' => $this->authService->getAuthData()['username'] ?? '',
                'request' => $options,
                'request_url' => $link,
                'request_headers' => $options['headers'] ?? [],
                'request_method' => $method,
                'response' => substr($resData, 0, 20000),
                'response_code' => $response->getStatusCode(),
                'response_headers' => $response->getHeaders(),
                'execute_time' => time() - $startTime,
                'request_content_length' => strlen((string)json_encode($options)),
                'response_content_length' => strlen($resData),
            ]);

            if (!$force && (empty($result) || !$result['success'])) {
                $this->authService->auth();
                return $this->request($method, $link, $options, true);
            }

            if (!empty($result)) {
                return $result;
            }
        } catch (ClientException $clientException) {
            throw new \Exception('Request returned error. ' . $clientException->getMessage(), $clientException->getCode());
        } catch (\Exception $exception) {
            throw new \Exception('Request returned error. ' . $exception->getMessage());
        }

        return null;
    }

    /**
     * @return Client
     * */
    public function getClient()
    {
        return new Client([
            'base_uri' => self::$LINK,
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => "Bearer {$this->authService->getToken()}"
            ],
        ]);
    }

    /** logging
     * @param string $message
     * @param array $context
     * @return void
     * */
    protected function logging($message, $context)
    {
        $logger = '\Log';
        if (class_exists($logger)) {
            $message = 'RICCIA // [' . ($this->authService->getAuthData()['username'] ?? '') . '] ' . $message;
            try {
                $logger::channel('another')->info($message, $context);
            } catch (\Exception $exception) {
                $logger::info($message, $context);
            }
        }
    }
}
