<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Core
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia\Core\Build;


/**
 *
 * */
class Builder
{
    /**
     * @var array $data
     * */
    protected $data;

    /**
     * @param array $whereData
     * @return void
     * */
    public function __construct(array $whereData = null)
    {
        $this->data = $whereData ?? [];
    }

    /** where builder cl
     * @param string $key
     * @param mixed $value
     * @return $this
     * */
    public function where($key, $value)
    {
        $this->data[$key] = $value;
        return $this;
    }

    /** clear filter
     * @return $this
     * */
    public function clear()
    {
        $this->data = [];
        return $this;
    }

    /** get filter
     * @return array
     * */
    public function getResult()
    {
        return $this->data;
    }

    /** has key property
     * @param string $key
     * @return bool
     * */
    public function has($key)
    {
        return !empty($this->data[$key]);
    }

    /** mixed property where
     * @param string $name
     * @param array $arguments
     * @return $this
     * */
    public function __call($name, $arguments)
    {
        $this->where($name, ...$arguments);
        return $this;
    }
}