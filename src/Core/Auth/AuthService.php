<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Auth
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia\Core\Auth;


use Riccia\Core\Query\HttpClient;

class AuthService
{
    /**
     * @var HttpClient $httpClient
     * */
    protected $httpClient;
    /**
     * @var array $authData
     * */
    protected $authData;
    /**
     * @var string $authPath
     * */
    protected static $authPath = 'storage/riccia/auth';
    /**
     * @var string $entryAuth
     * */
    protected static $entryAuth = 'sites';

    /**
     * @param HttpClient $httpClient
     * @param array $auth
     * @return void
     * */
    public function __construct(HttpClient $httpClient, array $auth)
    {
        $this->storeCookiesDir();
        $this->httpClient = $httpClient;
        $this->authData = $auth;
    }

    /**
     * @return array
     * @throws \Exception
     * */
    public function auth()
    {
        try {
            $result = $this->httpClient->request('POST', self::$entryAuth, ['json' => $this->getPrepareAuthData()], true);

            if (isset($result['success'], $result['content']) && $result['success']) {
                $this->writeAuthData($result);
                return $result;
            }

            return [];
        } catch (\Exception $exception) {
            throw new \Exception('Invalid Auth result. ' . $exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @return array
     * */
    public function getAuthData()
    {
        return $this->authData;
    }

    /**
     * @return array
     * */
    protected function getPrepareAuthData()
    {
        return [
            'username' => $this->authData['username'],
            'password' => base64_encode($this->authData['password']),
        ];
    }

    /**
     * @return string|null
     * */
    public function getToken()
    {
        return $this->readAuthData()['content']['access_token'] ?? null;
    }

    /**
     * @return array
     * */
    public function readAuthData()
    {
        $file = $this->authFile();
        if (file_exists($file)) {
            return json_decode((string)file_get_contents($file), true);
        }

        return [];
    }

    /**
     * @param array $data
     * @return bool
     * */
    protected function writeAuthData(array $data)
    {
        $file = $this->authFile();
        if (file_exists($file) && !is_writable($file)) {
            throw new \RuntimeException('FIle [' . $file . '] is not writable. Permission denied.');
        }

        $fp = fopen($file, "w");
        if ($fp) {
            $status = fwrite($fp, (string)json_encode($data));
            fclose($fp);
        }

        return (bool)($status ?? false);
    }

    /**
     * @return string
     * */
    protected function authFile()
    {
        $file = self::$authPath . DIRECTORY_SEPARATOR . $this->authData['username'] . '.json';
        if (file_exists($file)) {
            return $file;
        }

        if ($fs = fopen($file, 'x')) {
            fclose($fs);
            chmod($file, 0664);
        }

        return $file;
    }

    /**
     * @return void
     * @throws \RuntimeException
     * */
    protected function storeCookiesDir()
    {
        if (!file_exists(self::$authPath)) {
            if (!mkdir(self::$authPath, 0775, true)) {
                throw new \RuntimeException('Did\'t stored path for riccia auth. Permission denied.');
            }
        }
    }
}