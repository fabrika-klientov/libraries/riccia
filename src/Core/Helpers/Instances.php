<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Core
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia\Core\Helpers;


use Illuminate\Support\Str;

/**
 * @property-read \Riccia\Models\Orders $orders
 * @property-read \Riccia\Models\Items $items
 *
 * */
trait Instances
{

    /** getter magic instances
     * @param string $name
     * @return \Riccia\Models\Model|null
     * */
    public function __get($name)
    {
        switch ($name) {
            case 'orders':
            case 'items':
                $class = 'Riccia\\Models\\' . Str::ucfirst($name);
                return new $class($this->httpClient);
        }

        return null;
    }
    
}