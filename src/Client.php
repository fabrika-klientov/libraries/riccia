<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Riccia
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia;


use Riccia\Core\Helpers\Instances;
use Riccia\Core\Query\HttpClient;

class Client
{
    use Instances;

    /**
     * @var HttpClient $httpClient
     * */
    private $httpClient;

    /**
     * @param array $auth ['username' => string, 'password' => string]
     * @return void
     * */
    public function __construct(array $auth)
    {
        if ($this->validateAuth($auth)) {
            $this->httpClient = new HttpClient($auth);
        }
    }

    /**
     * @return HttpClient
     * */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /** validator for auth data
     * @param array $auth
     * @return bool
     * @throws \Exception
     * */
    protected function validateAuth(array $auth)
    {
        if (empty($auth)) {
            throw new \Exception('Auth params is empty');
        }

        if (!empty($auth['username']) &&
            is_string($auth['username']) &&
            !empty($auth['password']) &&
            is_string($auth['password'])
        ) {
            return true;
        }

        throw new \Exception('Auth params [username] and [password] is required');
    }
}