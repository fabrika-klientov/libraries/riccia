<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adapters
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia\Adapters;


use Riccia\Client;
use Riccia\Models\Orders;

class OrdersAdapter
{
    /**
     * @var Orders $data
     * */
    protected $data;
    /**
     * @var Client|null $client
     * */
    protected $client;

    /**
     * @param Orders $data
     * @param Client $client
     * @return void
     * */
    public function __construct(Orders $data, Client $client = null)
    {
        $this->data = $data;
        $this->client = $client;
    }

    /** id order
     * @override
     * @return int
     * */
    public function getOrderId()
    {
        return $this->data->id;
    }

    /** date_created order
     * @override
     * @return string
     * */
    public function getDateCreated()
    {
        return $this->data->created;
    }

    /** client_first_name client
     * @override
     * @return string|null
     * */
    public function getFirstName()
    {
        $full = $this->data->user['contact_fio'] ?? null;
        return isset($full) ? (explode(' ', $full)[1] ?? null) : null;
    }

    /** client_second_name client
     * @override
     * @return string|null
     * */
    public function getSecondName()
    {
        $full = $this->data->user['contact_fio'] ?? null;
        return isset($full) ? (explode(' ', $full)[2] ?? null) : null;
    }

    /** client_last_name client
     * @override
     * @return string|null
     * */
    public function getLastName()
    {
        $full = $this->data->user['contact_fio'] ?? null;
        return isset($full) ? (explode(' ', $full)[0] ?? null) : null;
    }

    /** full_name client
     * @override
     * @param bool $withSecondName
     * @return string|null
     * */
    public function getFullName(bool $withSecondName = true)
    {
        $full = $this->data->user['contact_fio'] ?? null;
        if (isset($full)) {
            $elements = explode(' ', $full);
            return count($elements) > 2 && !$withSecondName ? "$elements[0] $elements[1]" : $full;
        }

        return null;
    }

    /** email client
     * @override
     * @return string
     * */
    public function getEmail()
    {
        return $this->data->user['email'] ?? null;
    }

    /** phone client
     * @override
     * @return string
     * */
    public function getPhone()
    {
        return $this->data->user_phone;
    }

    /** delivery
     * @override
     * @return string
     * */
    public function getDeliveryId()
    {
        return $this->data->delivery['delivery_service_id'] ?? null;
    }

    /** delivery
     * @override
     * @return string
     * */
    public function getDeliveryType()
    {
        return $this->data->delivery['delivery_service_name'] ?? null;
    }

    /** delivery
     * @override
     * @return string|null
     * */
    public function getDeliveryAddress()
    {
        $city = $this->data->delivery['city'] ?? [];

        return ($city['region_title'] ?? '') . ', '
            . ($city['name'] ?? '') . ', '
            . ($this->data->delivery['place_street'] ?? '') . ', '
            . ($this->data->delivery['place_number'] ?? '') . ', '
            . ($this->data->delivery['place_house'] ?? '');
    }

    /** payment
     * @override
     * @return string
     * */
    public function getPaymentId()
    {
        return $this->data->payment_type;
    }

    /** payment
     * @override
     * @return string
     * */
    public function getPaymentType()
    {
        return $this->data->payment_type_name;
    }

    /** price
     * @override
     * @return string
     * */
    public function getPrice()
    {
        return $this->data->amount;
    }

    /** comment notes
     * @override
     * @return string
     * */
    public function getComment()
    {
        return $this->data->comment;
    }

    /** products
     * @override
     * @return \Illuminate\Support\Collection
     * */
    public function getProducts()
    {
        return collect(array_map(function ($item) {
            return new OrdersProductAdapter($item, $this->client);
        }, $this->data->purchases ?? []));
    }

    /** status
     * @override
     * @return string
     * */
    public function getStatus()
    {
        return $this->data->status_data['name'] ?? null;
    }

    /** source
     * @override
     * @return string
     * */
    public function getSource()
    {
        return 'rozetka.ua';
    }

}