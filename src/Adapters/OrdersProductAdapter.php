<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adapters
 * @category  Riccia
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.28
 * @link      https://fabrika-klientov.ua
 */

namespace Riccia\Adapters;


use Riccia\Client;

class OrdersProductAdapter
{
    /**
     * @var array $data
     * */
    protected $data;
    /**
     * @var Client|null $client
     * */
    protected $client;

    /**
     * @param array $data
     * @param Client $client
     * @return void
     * */
    public function __construct(array $data, Client $client = null)
    {
        $this->data = $data;
        $this->client = $client;
    }

    /** id
     * @override
     * @return string
     * */
    public function getId()
    {
        return $this->data['item']['id'];
    }

    /** name
     * @override
     * @return string
     * */
    public function getName()
    {
        return $this->data['item_name'];
    }

    /** price
     * @override
     * @return string
     * */
    public function getPrice()
    {
        return $this->data['price'];
    }

    /** quantity
     * @override
     * @return string
     * */
    public function getQuantity()
    {
        return $this->data['quantity'];
    }

    /** total_price
     * @override
     * @return string
     * */
    public function getTotalPrice()
    {
        return $this->data['cost'];
    }

    /** url
     * @override
     * @return string
     * */
    public function getLink()
    {
        return $this->data['item']['url'];
    }

    /** measure_unit (шт. упаковка.)
     * @override
     * @return string|null
     * */
    public function getMeasureUnit()
    {
        return null; // not found
    }

    /** article
     * @override
     * @return string|null
     * */
    public function getArticle()
    {
        return $this->data['item']['article'] ?? null;
    }

}