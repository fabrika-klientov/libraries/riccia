<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 28.02.20
 * Time: 10:22
 */

use Riccia\Client;
use PHPUnit\Framework\TestCase;
use Riccia\Core\Query\HttpClient;

class ClientTest extends TestCase
{

    public function test__construct()
    {
        $client = new Client(['username' => 'user', 'password' => 'pass']);

        $this->assertInstanceOf(Client::class, $client);

        $this->expectException(\Exception::class);

        new Client([]);
        new Client(['username' => 'user']);
        new Client(['password' => 'pass']);
    }

    public function testGetHttpClient()
    {
        $client = new Client(['username' => 'user', 'password' => 'pass']);

        $this->assertInstanceOf(HttpClient::class, $client->getHttpClient());
    }
}
