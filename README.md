## Status

[![Latest Stable Version](https://poser.pugx.org/shadoll/riccia/v/stable)](https://packagist.org/packages/shadoll/riccia)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/riccia/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/riccia/commits/master)
[![coverage report](https://gitlab.com/fabrika-klientov/libraries/riccia/badges/master/coverage.svg)](https://gitlab.com/fabrika-klientov/libraries/riccia/commits/master)
[![License](https://poser.pugx.org/shadoll/riccia/license)](https://packagist.org/packages/shadoll/riccia/)

**[Rozetka](https://rozetka.ua/) PHP API Library**

---


## Install

`composer require shadoll/riccia`


**Не все методы реализованны**

```php

$client = new \Riccia\Client([
    "username" => "login",
    "password" => "pass...",
]);

// Коллекция заказов
/**
 * @var \Riccia\Core\Collection\Collection<\Riccia\Models\Orders> $collect
 **/
$collect = $client->orders->get();
// модели по умолчанию загружаются с максимальными связями с другими сущностями розетки
// чтобы указать конкретные 
$collect = $client->orders->expand(['user', 'delivery'])->get();
// с фильтрами (больше фильтров и сортировка смотрите в классе модели)
$collect = $client->orders->created_from('2019-12-24 00:00:00')->page(2)->get();

// Модель по ID
/**
 * @var \Riccia\Models\Orders $model
 **/
$model = $client->orders->find(96421289);

// Сводные данные по заказам
$arrayData = $client->orders->counts();

// Коллекция продуктов
/**
 * @var \Riccia\Core\Collection\Collection<\Riccia\Models\Products> $collect
 **/
$collect = $client->items->get();
// модели по умолчанию загружаются с максимальными связями с другими сущностями розетки
// чтобы указать конкретные 
$collect = $client->orders->expand(['status', 'description'])->get();
// с фильтрами (больше фильтров и сортировок смотрите в классе модели)
$collect = $client->items->find_by_text('jarvis')->get();

// Модель по ID
/**
 * @var \Riccia\Models\Products $model
 **/
$model = $client->items->find(12345);


```

**_Количество моделей и функций будут пополнятся_**